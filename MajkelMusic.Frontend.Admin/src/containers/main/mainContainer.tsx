import * as React from 'react';
import * as DOM from 'react-dom';
import styled from 'styled-components';

const Div = styled.div`
  color: red;
  background: blue;
`;

export class MainContainer extends React.Component<any, any> {
  render() {
    return <Div>Hello world!</Div>;
  }
}
