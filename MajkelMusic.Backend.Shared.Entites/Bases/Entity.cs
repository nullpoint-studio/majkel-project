﻿using MajkelMusic.Backend.Shared.Entites.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace MajkelMusic.Backend.Shared.Entites.Bases
{
    public class Entity : IEntity
    {
        public long Id { get; private set; }
        public DateTime CreationDate { get; private set; }
        public DateTime LastModified { get; set; }
    }
}
