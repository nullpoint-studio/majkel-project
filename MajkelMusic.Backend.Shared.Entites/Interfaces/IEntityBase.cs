﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MajkelMusic.Backend.Shared.Entites.Interfaces
{
    public interface IEntityBase<T>
    {
        T Id { get; }
        DateTime CreationDate { get; }
        DateTime LastModified { get; set; }
    }
}
