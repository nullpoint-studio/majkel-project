﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace MajkelMusic.Backend.Media.Entites.Models
{
    /// <summary>
    /// Medium is base class for any objects contains binary data.
    /// </summary>
    public class Medium
    {
        /// <summary>
        /// ID in database of this medium.
        /// If null then adds this medium as new to database.
        /// </summary>
        public long Id { get; private set; }

        /// <summary>
        /// Date of upload this medium to server.
        /// Cannot be seted.
        /// </summary>
        public DateTime UploadDate { get; private set; }

        /// <summary>
        /// Date of last edit.
        /// </summary>
        public DateTime LastEditDate { get; set; }

        /// <summary>
        /// Name of file on upload time.
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// Mimetype type ({type}/{subtype}) of this medium.
        /// </summary>
        public string MimeMaintype { get; set; }

        /// <summary>
        /// Mimetype subtype ({type}/{subtype}) of this medium.
        /// </summary>
        public string MimeSubtype { get; set; }

        /// <summary>
        /// Length of binary data of this medium.
        /// </summary>
        public long Length { get; set; }

        /// <summary>
        /// Binary contents of this medium.
        /// </summary>
        public byte[] Data { get; set; }
        
        /// <summary>
        /// Returns full mime (type and subtype) ({type}/{subtype}) type of this medium.
        /// </summary>
        [NotMapped]
        public string MimeType {
            get
            {
                return $"{this.MimeMaintype}/{this.MimeSubtype}";
            }
        }

        /// <summary>
        /// Checks for convergence of data types given in arguments with this medium mime type.
        /// </summary>
        /// <param name="mimeMaintype">Mime type</param>
        /// <param name="mimeSubtype">Mime subtype</param>
        /// <returns>Mime types are consistent.</returns>
        public bool Is(string mimeMaintype, string mimeSubtype = null)
        {
            return 
                this.MimeMaintype == mimeMaintype && 
                mimeSubtype == null ? true : mimeSubtype == this.MimeSubtype;
        }
    }
}
