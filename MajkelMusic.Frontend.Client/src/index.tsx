import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Router, Route, Switch } from 'react-router';
import { createBrowserHistory } from 'history';
import { MainContainer } from '@container/main';
import { Sample } from '@domain/main';

const history = createBrowserHistory();

ReactDOM.render(
  <Router history={history}>
    <Switch>
      <Route path="/" component={MainContainer} />
    </Switch>
  </Router>,
  document.getElementById('root')
);
